require "minitest/autorun"

class Multiplier
  def self.multiply(a,b)
    a * b
  end
end

class TestMultiplier < MiniTest::Unit::TestCase
  def test_2_2
    assert_equal 4, Multiplier.multiply(2,2)
  end

  def test_5_10
    assert_equal 50, Multiplier.multiply(5,10)
  end

  def test_fail
    assert_equal true, true
  end
end
